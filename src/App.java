public class App {
    public static void printArr(int[] nums, int r){
        System.out.print("[");
        for(int i=0;i<r;i++){
            System.out.print(nums[i]);
            if(i < r - 1){
                System.out.print(", ");
            }
        }
        System.out.println("]");
        
    }

    public static int removeElement(int[] nums){
        int k = 1;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != nums[k - 1]){
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) throws Exception {
       int nums1[] = {1,1,2};
       int r1 = removeElement(nums1);

       System.out.print(r1 + ", ");
       System.out.print("nums = ");
       printArr(nums1, r1);
//--------------------------------------------------------------
       int nums2[] = {0,0,1,1,1,2,2,3,3,4};
       int r2 = removeElement(nums2);
       System.out.print(r2 + ", ");
       System.out.print("nums = ");
       printArr(nums2, r2);
    }
}
